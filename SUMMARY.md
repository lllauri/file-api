# Summary

## Comments
Acceptance was easy to understand. Project setup required some
steps that were not documented (needed to create docker volume,
`./do.sh init` is not implemented)
The progress was very slow because everything was new to me.
Once I got to trying out file upload logic I didn't manage to
get it to work so I will probably not dedicate any more time to it
because I'm already at the time limit.

Project is incomplete and the code is alpha/not polished at all
because this is usually done after getting everything to work.

Resources used:

https://kotlinlang.org/docs/getting-started.html

https://swagger.io/docs/

https://www.mongodb.com/developer/languages/kotlin/spring-boot3-kotlin-mongodb/

https://medium.com/@mohammedharfan26/file-uploading-with-spring-kotlin-and-mongodb-3a180d2969ae


## Which part of the assignment took the most time and why?
Finding resources and documentation

## What You learned
This was my first time using Idea, Kotlin, Spring and MongoDB
so pretty much everything was new. The concepts and logic
were familiar.

## TODOs
The project is incomplete, so:
- finish API according to acceptance criteria
    - add support for all endpoints
    - add logging
    - better response handling
    - tests
    - refactor/polish the code
- midway through I found out that MongoDB document max limit is 16MB,
refactor file saving to use GridFS
- some kind of auth/permission system, at the moment you can access
the file if you know the token
- some kind of file routing/bucketing logic (hot/cold/big/small)
