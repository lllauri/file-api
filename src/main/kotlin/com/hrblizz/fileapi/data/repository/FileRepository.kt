package com.hrblizz.fileapi.data.repository

import org.springframework.data.mongodb.repository.MongoRepository
import com.hrblizz.fileapi.data.entities.FileEntity

interface FileRepository : MongoRepository<FileEntity, String> {
    fun findByToken(token: String): FileEntity?
}