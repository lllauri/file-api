package com.hrblizz.fileapi.data.services

import com.hrblizz.fileapi.data.entities.Content
import com.hrblizz.fileapi.data.entities.FileEntity
import com.hrblizz.fileapi.data.repository.FileRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.time.LocalDateTime
import java.util.*

@Service
class FileService(@Autowired val repo: FileRepository) {
    fun getFileContentByToken(token: String): Content? {
        return repo.findByToken(token)?.content
    }

    fun getFileMetaByToken(token: String): FileEntity? {
        return repo.findByToken(token)
    }

    fun saveFile(
        name: String?,
        contentType: String?,
        source: String?,
        expireTime: LocalDateTime?,
        additionalInfo: String?,
        content: MultipartFile?,
    ): FileEntity? {
        val token = UUID.randomUUID().toString()
        val size = content?.size
        val createTime = LocalDateTime.now()
        val encodedContent = encodeImageAsBase64(content)

        val file = FileEntity(
            token = token,
            name = name,
            contentType = contentType,
            source = source,
            expireTime = expireTime,
            createTime = createTime, // probably built into mongo
            size = size,
            meta = additionalInfo,
            content = Content(token = token, content = encodedContent)
        )
        return file.let { repo.save(it) }
    }

    fun deleteByToken(token: String): Boolean {
        return repo.existsById(token) && runCatching { repo.deleteById(token) }.isSuccess
    }

    private fun encodeImageAsBase64(image: MultipartFile?): String? {
        return image?.let {
            val imageBytes = it.bytes
            Base64.getEncoder().encodeToString(imageBytes)
        }
    }
}