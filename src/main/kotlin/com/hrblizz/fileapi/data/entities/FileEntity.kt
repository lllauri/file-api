package com.hrblizz.fileapi.data.entities

import com.fasterxml.jackson.annotation.JsonInclude
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document("files")
@JsonInclude(JsonInclude.Include.NON_NULL)
data class FileEntity(
    @Id
    val token: String,
    val name: String?,
    val contentType: String?,
    val source: String?,
    val meta: String?,
    val expireTime: LocalDateTime?,
    val createTime: LocalDateTime?,
    val content: Content,
    val size: Long?,
)
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Content (
    @Id
    val token: String,
    val content: String?,
)

