package com.hrblizz.fileapi.controller

import com.hrblizz.fileapi.data.services.FileService
import com.hrblizz.fileapi.rest.ResponseEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.time.LocalDateTime

const val MULTIPART_FORM_DATA = "multipart/form-data"

@RestController
class FileController(@Autowired val service: FileService) {
    @GetMapping("/file/{token}")
    fun getFileByToken(@PathVariable("token") token: String): ResponseEntity<out Any> {
        return service.getFileContentByToken(token)?.let {
            ResponseEntity(it, HttpStatus.OK.value())
        } ?: ResponseEntity(HttpStatus.NOT_FOUND.value())
    }

    @DeleteMapping("/file/{token}")
    fun deleteSchema(@PathVariable("token") token: String): ResponseEntity<out Any> =
        service.deleteByToken(token).run {
            if (this) ResponseEntity(HttpStatus.OK.value()) else ResponseEntity(HttpStatus.NOT_FOUND.value())
        }

    @PostMapping("/files", consumes = [MULTIPART_FORM_DATA])
    fun saveSchema(
        @RequestParam("content") content: MultipartFile?,
        @RequestPart("name") name: String?,
        @RequestPart("contentType") contentType: String?,
        @RequestPart("source") source: String?,
        @RequestPart("expireTime") expireTime: String?,
        @RequestPart("meta") additionalInfo: String?,
        ): ResponseEntity<out Any> {
        val parsedExpireTime = expireTime?.let { LocalDateTime.parse(it) }
        service.saveFile(
            name,
            contentType,
            source,
            parsedExpireTime,
            additionalInfo,
            content)?.let {
                return ResponseEntity("{ \"token\": $it }", HttpStatus.CREATED.value())
        }
        return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR.value())
    }
}